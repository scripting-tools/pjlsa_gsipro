import pjlsa


class LSAClientGSI(pjlsa.BaseLSAClient):
    def __init__(self) -> None:
        """Connect to the production LSA server at GSI."""
        cfg_url = "http://asl156.acc.gsi.de:58080/application/profile/pro/"
        super().__init__(
            "gsi-pro",
            {
                "csco.default.property.config.url": cfg_url,
                "log4j.configurationFile": "log4j2-pro.xml",
            },
        )
