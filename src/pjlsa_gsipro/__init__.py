__version__ = "1.1.1"

__gradle_deps__ = [
    {
        "groupId": "de.gsi.lsa.core",
        "product": "lsa-client-gsi",
        "version": "19.2.0",
        "repository": 'maven { url "https://artifacts.acc.gsi.de/repository/default/" }',
    },
    {
        "groupId": "de.gsi.aco.app",
        "product": "acoapp-third-party-bom",
        "version": "19.2.0",
        "type": "enforcedPlatform",
        "repository": 'maven { url "https://artifacts.acc.gsi.de/repository/default/" }',
    },
]

__all__ = ["LSAClientGSI"]

from .lsa_client import LSAClientGSI
